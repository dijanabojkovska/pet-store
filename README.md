Project Name: 

    Pet Store

Description:

    The Pet Store is application where users can interact with pets, which can be either Cats or Dogs. Users have the ability to perform the following actions:

    Create individual pets (Cat or Dog).
    Generate a random number of pets (between 1 and 10).
    Retrieve a pet by its unique identifier (ID).
    Retrieve a list of all pets available at the store.

    Create user profiles.
    Generate a random number of users (between 1 and 10) with user-friendly information (generated using the Faker library).
    Retrieve a user by their unique identifier (ID).
    Retrieve a list of all users.
    Additionally, users have the option to browse through all available pets in the store and purchase any pets they have the budget for.
    The following conditions must be met for a successful purchase:
        The selected pet must not have an owner.
        The user must have sufficient budget.


Technology Stack:

    Java 17
    Spring Boot 3.1.4
    Maven
    PostgreSQL as the database
    Spring Data JPA for data persistence
    Lombok to reduce boilerplate code
    Mapstruct for mapping purposes
    Faker library for generating user-friendly information
    JUnit 5
    Mockito

Database Configuration

    The application uses PostgreSQL as the database.
    Database configuration details can be found in the application.properties file.

Installation

    To get started with the Pet Store application, follow these steps:
    Clone the repository to your local machine:

    bash
    Copy code
    git clone https://gitlab.com/dijanabojkovska/pet-store
    Navigate to the project directory:

    Build the project using Maven:

REST API

    Once the application is running, you can interact with it using the provided API endpoints.
    The REST API endpoints have been tested with Postman.
    There is exported PetStore.postman_collection.json file, with all REST API endpoints used and tested.

