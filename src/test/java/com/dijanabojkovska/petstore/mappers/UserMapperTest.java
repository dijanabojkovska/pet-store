package com.dijanabojkovska.petstore.mappers;

import com.dijanabojkovska.petstore.dtos.requests.UserRequest;
import com.dijanabojkovska.petstore.dtos.responses.UserResponse;
import com.dijanabojkovska.petstore.models.User;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static com.dijanabojkovska.petstore.ObjectFactory.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class UserMapperTest {

    private final UserMapper userMapper = new UserMapperImpl();

    @Test
    void mapUserToUserResponse_ShouldMapCorrectly() {
        // Arrange
        User user = createUser();

        // Act
        UserResponse userResponse = userMapper.mapUserToUserResponse(user);

        // Assert
        assertEquals(UserResponse.class, userResponse.getClass());
        assertEquals(user.getId(), userResponse.getId());
        assertEquals(user.getFirstName(), userResponse.getFirstName());
        assertEquals(user.getLastName(), userResponse.getLastName());
        assertEquals(user.getEmail(), userResponse.getEmail());
    }

    @Test
    void mapUserRequestToUser_ShouldMapCorrectly() {
        // Arrange
        UserRequest userRequest = createUserRequest();

        // Act
        User user = userMapper.mapUserRequestToUser(userRequest);

        // Assert
        assertEquals(User.class, user.getClass());
        assertEquals(userRequest.getFirstName(), user.getFirstName());
        assertEquals(userRequest.getLastName(), user.getLastName());
        assertEquals(userRequest.getEmail(), user.getEmail());
    }

    @Test
    void mapUserListToUserResponseList_ShouldMapCorrectly() {
        // Arrange
        List<User> userList = createUserList();

        // Act
        List<UserResponse> userResponseList = userMapper.mapUserListToUserResponseList(userList);

        // Assert
        assertEquals(UserResponse.class, userResponseList.get(0).getClass());
        assertEquals(UserResponse.class, userResponseList.get(1).getClass());
        assertEquals(userList.get(0).getId(), userResponseList.get(0).getId());
        assertEquals(userList.get(1).getId(), userResponseList.get(1).getId());
        assertEquals(userList.get(0).getFirstName(), userResponseList.get(0).getFirstName());
        assertEquals(userList.get(1).getFirstName(), userResponseList.get(1).getFirstName());
        assertEquals(userList.get(0).getLastName(), userResponseList.get(0).getLastName());
        assertEquals(userList.get(1).getLastName(), userResponseList.get(1).getLastName());
        assertEquals(userList.get(0).getEmail(), userResponseList.get(0).getEmail());
        assertEquals(userList.get(1).getEmail(), userResponseList.get(1).getEmail());
    }
}
