package com.dijanabojkovska.petstore.mappers;

import com.dijanabojkovska.petstore.dtos.requests.PetRequest;
import com.dijanabojkovska.petstore.dtos.responses.PetResponse;
import com.dijanabojkovska.petstore.models.Cat;
import com.dijanabojkovska.petstore.models.Dog;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;

import static com.dijanabojkovska.petstore.ObjectFactory.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class PetMapperTest {

    private final PetMapper petMapper = new PetMapperImpl();

    @Test
    void mapPetRequestToDog_ShouldMapRequestToDogCorrectly() {
        // Arrange
        PetRequest petRequest = createDogRequest();

        // Act
        Dog dog = petMapper.mapPetRequestToDog(petRequest);

        // Assert
        assertEquals(Dog.class, dog.getClass());
        assertEquals(petRequest.getName(), dog.getName());
        assertEquals(petRequest.getDescription(), dog.getDescription());
        assertEquals(LocalDate.parse(petRequest.getBirthDate()), dog.getBirthDate());
        assertEquals(petRequest.getRating(), dog.getRating());
    }


    @Test
    void mapPetRequestToCat_ShouldMapRequestToCatCorrectly() {
        // Arrange
        PetRequest petRequest = createCatRequest();

        // Act
        Cat cat = petMapper.mapPetRequestToCat(petRequest);

        // Assert
        assertEquals(Cat.class, cat.getClass());
        assertEquals(petRequest.getName(), cat.getName());
        assertEquals(petRequest.getDescription(), cat.getDescription());
        assertEquals(LocalDate.parse(petRequest.getBirthDate()), cat.getBirthDate());
    }

    @Test
    void mapCatToPetResponse_ShouldMapCatToResponseCorrectly() {
        // Arrange
        Cat cat = createCat();

        // Act
        PetResponse petResponse = petMapper.mapCatToPetResponse(cat);

        // Assert
        assertEquals(PetResponse.class, petResponse.getClass());
        assertEquals(petResponse.getName(), cat.getName());
        assertEquals(petResponse.getDescription(), cat.getDescription());
        assertEquals(LocalDate.parse(petResponse.getBirthDate()), cat.getBirthDate());
    }

    @Test
    void mapDogToPetResponse_ShouldMapDogToResponseCorrectly() {
        // Arrange
        Dog dog = createDog();

        // Act
        PetResponse petResponse = petMapper.mapDogToPetResponse(dog);

        // Assert
        assertEquals(PetResponse.class, petResponse.getClass());
        assertEquals(PetResponse.class, petResponse.getClass());
        assertEquals(petResponse.getName(), dog.getName());
        assertEquals(petResponse.getDescription(), dog.getDescription());
        assertEquals(LocalDate.parse(petResponse.getBirthDate()), dog.getBirthDate());
        assertEquals(petResponse.getRating(), dog.getRating());
    }

    @Test
    void  mapPetToPetResponse_WithDog_ShouldMapDogToResponseCorrectly() {
        // Arrange
        Dog dog = createDog();

        // Act
        PetResponse petResponse = petMapper.mapPetToPetResponse(dog);

        // Assert
        assertEquals(PetResponse.class, petResponse.getClass());
        assertEquals(PetResponse.class, petResponse.getClass());
        assertEquals(petResponse.getName(), dog.getName());
        assertEquals(petResponse.getDescription(), dog.getDescription());
        assertEquals(LocalDate.parse(petResponse.getBirthDate()), dog.getBirthDate());
        assertEquals(petResponse.getRating(), dog.getRating());
    }

    @Test
    void mapPetToPetResponse_WithCat_ShouldMapCatToResponseCorrectly() {
        // Arrange
        Cat cat = createCat();

        // Act
        PetResponse petResponse = petMapper.mapPetToPetResponse(cat);

        // Assert
        assertEquals(PetResponse.class, petResponse.getClass());
        assertEquals(PetResponse.class, petResponse.getClass());
        assertEquals(petResponse.getName(), cat.getName());
        assertEquals(petResponse.getDescription(), cat.getDescription());
        assertEquals(LocalDate.parse(petResponse.getBirthDate()), cat.getBirthDate());
    }
}
