package com.dijanabojkovska.petstore.services;

import com.dijanabojkovska.petstore.ObjectFactory;
import com.dijanabojkovska.petstore.dtos.requests.PetRequest;
import com.dijanabojkovska.petstore.dtos.responses.PetResponse;
import com.dijanabojkovska.petstore.mappers.PetMapper;
import com.dijanabojkovska.petstore.models.Cat;
import com.dijanabojkovska.petstore.models.Dog;
import com.dijanabojkovska.petstore.models.Pet;
import com.dijanabojkovska.petstore.models.User;
import com.dijanabojkovska.petstore.repositories.HistoryLogRepository;
import com.dijanabojkovska.petstore.repositories.PetRepository;
import com.dijanabojkovska.petstore.repositories.UserRepository;
import com.dijanabojkovska.petstore.services.impl.PetServiceImpl;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.*;

import static com.dijanabojkovska.petstore.ObjectFactory.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class PetServiceImplTest {
    @Mock
    private PetMapper petMapper;
    @Mock
    private PetRepository petRepository;
    @Mock
    private UserRepository userRepository;
    @Mock
    private HistoryLogRepository historyLogRepository;

    private PetService petService;

    @BeforeEach
    public void setUp() {
        petService = new PetServiceImpl(petRepository, userRepository, historyLogRepository, petMapper);
    }

    @AfterEach
    public void tearAfter() {
        petService = null;
    }

    @Test
    public void getPetById_ExistingDog_ShouldReturnDogResponse() {
        // Arrange
        Dog dog = createDog();
        PetResponse dogResponse = createDogResponse();

        when(petRepository.findById(ID)).thenReturn(Optional.of(dog));
        when(petMapper.mapDogToPetResponse(dog)).thenReturn(dogResponse);

        // Act
        Optional<PetResponse> actualDogResponse = petService.getPetById(ID);

        // Assert
        assertTrue(actualDogResponse.isPresent());
        assertEquals(dog.getId(), actualDogResponse.get().getId());
        assertEquals(dog.getName(), actualDogResponse.get().getName());
        assertEquals(dog.getDescription(), actualDogResponse.get().getDescription());
        assertEquals(dog.getRating(), actualDogResponse.get().getRating());
        assertEquals(0, dog.calculatePrice().compareTo(actualDogResponse.get().getPrice()));
        assertNull(actualDogResponse.get().getOwner());
    }

    @Test
    public void getPetById_ExistingCat_ShouldReturnCatResponse() {
        // Arrange
        Cat cat = createCat();
        PetResponse catResponse = createCatResponse();

        when(petRepository.findById(ID)).thenReturn(Optional.of(cat));
        when(petMapper.mapCatToPetResponse(cat)).thenReturn(catResponse);

        // Act
        Optional<PetResponse> actualCatResponse = petService.getPetById(ID);

        // Assert
        assertTrue(actualCatResponse.isPresent());
        assertEquals(cat.getId(), actualCatResponse.get().getId());
        assertEquals(cat.getName(), actualCatResponse.get().getName());
        assertEquals(cat.getDescription(), actualCatResponse.get().getDescription());
        assertEquals(0, cat.calculatePrice().compareTo(actualCatResponse.get().getPrice()));
        assertNull(actualCatResponse.get().getOwner());
    }

    @Test
    public void getPetById_NonExistingPetId_ShouldThrowNoSuchElementException() {
        // Arrange
        when(petRepository.findById(NON_EXISTING_ID)).thenReturn(Optional.empty());

        // Act & Assert
        assertThrows(NoSuchElementException.class, () -> petService.getPetById(NON_EXISTING_ID));
    }

    @Test
    void getAllPets_ReturnsListOfPetResponses() {
        List<Pet> petList = createPetList();
        List<PetResponse> petResponseList = createPetResponseList();

        when(petRepository.findAll()).thenReturn(petList);
        when(petMapper.mapCatsAndDogsListToPetResponseList(petList)).thenReturn(petResponseList);

        Collection<PetResponse> actualPetResponseList =
                petService.getAllPets().stream().toList();

        assertEquals(petResponseList, actualPetResponseList);
    }

    @Test
    void getAllPets_EmptyList_ShouldReturnEmptyResponseList() {
        // Arrange
        when(petRepository.findAll()).thenReturn(Collections.emptyList());

        // Act
        Collection<PetResponse> actualPetResponseList = petService.getAllPets();

        // Assert
        assertTrue(actualPetResponseList.isEmpty());
    }

    @Test
    void createPet_ValidDogRequest_ShouldReturnDogResponse() {
        //Arrange
        PetRequest dogRequest = createDogRequest();
        Dog dog = createDog();
        PetResponse dogResponse = createDogResponse();

        when(petMapper.mapPetRequestToDog(dogRequest)).thenReturn(dog);
        when(petRepository.save(petMapper.mapPetRequestToDog(dogRequest)))
                .thenReturn(dog);
        when(petMapper.mapDogToPetResponse(dog)).thenReturn(dogResponse);

        //Act
        PetResponse actualDogResponse = petService.createPet(dogRequest);

        //Assert
        assertEquals(dogResponse.getName(), actualDogResponse.getName());
        assertEquals(dogResponse.getDescription(), actualDogResponse.getDescription());
        assertEquals(dogResponse.getBirthDate(), actualDogResponse.getBirthDate());
        assertEquals(0, dogResponse.getPrice().compareTo(actualDogResponse.getPrice()));
        assertEquals(dogResponse.getRating(), actualDogResponse.getRating());
        assertNull(actualDogResponse.getOwner());
    }

    @Test
    void createPet_ValidCatRequest_ShouldReturnCatResponse() {
        //Arrange
        PetRequest catRequest = createCatRequest();
        Cat cat = createCat();
        PetResponse catResponse = createCatResponse();

        when(petMapper.mapPetRequestToCat(catRequest)).thenReturn(cat);
        when(petRepository.save(petMapper.mapPetRequestToCat(catRequest)))
                .thenReturn(cat);
        when(petMapper.mapCatToPetResponse(cat)).thenReturn(catResponse);

        //Act
        PetResponse actualCatResponse = petService.createPet(catRequest);

        //Assert
        assertEquals(catResponse.getName(), actualCatResponse.getName());
        assertEquals(catResponse.getDescription(), actualCatResponse.getDescription());
        assertEquals(catResponse.getBirthDate(), actualCatResponse.getBirthDate());
        assertEquals(0, catResponse.getPrice().compareTo(actualCatResponse.getPrice()));
        assertEquals(catResponse.getRating(), actualCatResponse.getRating());
        assertNull(actualCatResponse.getOwner());
    }

    @Test
    void createRandomPets_UsingMockedRandom_ShouldSaveAndMapPetResponses() throws NoSuchFieldException, IllegalAccessException {
        Random random = mock(Random.class);
        when(random.nextBoolean()).thenReturn(true);
        when(random.nextInt(anyInt())).thenReturn(5);

        Field randomField = PetServiceImpl.class.getDeclaredField(RANDOM);
        randomField.setAccessible(true);
        randomField.set(petService, random);

        // Act
        List<PetResponse> petResponses = (List<PetResponse>) petService.createRandomPets();

        // Assert
        assertNotNull(petResponses);

        verify(petRepository, times(6)).save(any(Pet.class));
        verify(petMapper, times(6)).mapPetToPetResponse(any(Pet.class));
    }


    @Test
    void buyPet_ShouldAssignOwnersAndDeductBudgets() {
        // Arrange
        List<Pet> petList = new ArrayList<>();
        List<User> userList = new ArrayList<>();

        when(petRepository.findAll()).thenReturn(petList);
        when(userRepository.findAll()).thenReturn(userList);

        User user = ObjectFactory.createUser();
        userList.add(user);
        Cat cat = ObjectFactory.createCat();
        petList.add(cat);
        Dog dog = ObjectFactory.createDog();
        petList.add(dog);

        //Act
        petService.buy();

        //Assert
        assertEquals(user, cat.getOwner());
        assertEquals(user, dog.getOwner());
        assertEquals(BigDecimal.valueOf(35), user.getBudget());

        verify(userRepository, times(2)).save(user);
        verify(petRepository, times(2)).save(any());
    }
}

