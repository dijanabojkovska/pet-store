package com.dijanabojkovska.petstore.services;

import com.dijanabojkovska.petstore.dtos.requests.UserRequest;
import com.dijanabojkovska.petstore.dtos.responses.UserResponse;
import com.dijanabojkovska.petstore.mappers.UserMapper;
import com.dijanabojkovska.petstore.models.User;
import com.dijanabojkovska.petstore.repositories.UserRepository;
import com.dijanabojkovska.petstore.services.impl.UserServiceImpl;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.lang.reflect.Field;
import java.util.*;

import static com.dijanabojkovska.petstore.ObjectFactory.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class UserServiceImplTest {
    @Mock
    private UserRepository userRepository;
    @Mock
    private UserMapper userMapper;
    private UserService userService;

    @BeforeEach
    public void setUp() {
        userService = new UserServiceImpl(userRepository, userMapper);
    }

    @AfterEach
    public void tearAfter() {
        userService = null;
    }

    @Test
    void getUserById_ExistingUser_ShouldReturnUserResponse() {
        // Arrange
        User user = createUser();
        UserResponse userResponse = createUserResponse();
        when(userRepository.findById(ID)).thenReturn(Optional.of(user));
        when(userMapper.mapUserToUserResponse(user)).thenReturn(userResponse);

        // Act
        Optional<UserResponse> actualUserResponse = userService.getUserById(ID);

        // Assert
        assertTrue(actualUserResponse.isPresent());
        assertEquals(user.getId(), actualUserResponse.get().getId());
        assertEquals(user.getFirstName(), actualUserResponse.get().getFirstName());
        assertEquals(user.getLastName(), actualUserResponse.get().getLastName());
        assertEquals(user.getEmail(), actualUserResponse.get().getEmail());
        assertEquals(user.getBudget(), actualUserResponse.get().getBudget());
    }

    @Test
    void getUserById_NonExistingUserId_ShouldThrowNoSuchElementException() {
        // Arrange
        when(userRepository.findById(NON_EXISTING_ID)).thenReturn(Optional.empty());

        // Act & Assert
        assertThrows(NoSuchElementException.class, () -> userService.getUserById(NON_EXISTING_ID));
    }

    @Test
    void getAllUsers_ReturnsListOfUserResponses() {
        List<User> userList = createUserList();
        List<UserResponse> userResponseList = createUserResponseList();

        when(userRepository.findAll()).thenReturn(userList);
        when(userMapper.mapUserListToUserResponseList(userList)).thenReturn(userResponseList);

        Collection<UserResponse> actualUserResponseList =
                userService.getAllUsers().stream().toList();

        assertEquals(userResponseList, actualUserResponseList);
    }

    @Test
    void getAllUsers_EmptyList_ShouldReturnEmptyResponseList() {
        // Arrange
        when(userRepository.findAll()).thenReturn(Collections.emptyList());

        // Act
        Collection<UserResponse> actualUserResponseList = userService.getAllUsers();

        // Assert
        assertTrue(actualUserResponseList.isEmpty());
    }

    @Test
    void createUser_ValidUserRequest_ShouldReturnUserResponse() {
        //Arrange
        UserRequest userRequest = createUserRequest();
        User user = createUser();
        UserResponse userResponse = createUserResponse();

        when(userMapper.mapUserRequestToUser(userRequest)).thenReturn(user);
        when(userRepository.save(userMapper.mapUserRequestToUser(userRequest)))
                .thenReturn(user);
        when(userMapper.mapUserToUserResponse(user)).thenReturn(userResponse);

        //Act
        UserResponse actualUserResponse = userService.createUser(userRequest);

        //Assert
        assertEquals(userRequest.getFirstName(), actualUserResponse.getFirstName());
        assertEquals(userRequest.getLastName(), actualUserResponse.getLastName());
        assertEquals(userRequest.getEmail(), actualUserResponse.getEmail());
        assertEquals(userRequest.getBudget(), actualUserResponse.getBudget());
    }

    @Test
    void createRandomUsers_UsingMockedRandom_ShouldSaveAndMapUserResponses() throws NoSuchFieldException, IllegalAccessException {
        Random random = mock(Random.class);
        when(random.nextInt(anyInt())).thenReturn(5);

        Field randomField = UserServiceImpl.class.getDeclaredField(RANDOM);
        randomField.setAccessible(true);
        randomField.set(userService, random);
        when(userRepository.save(any(User.class))).thenReturn(new User());

        Collection<UserResponse> userResponses = userService.createRandomUsers();

        assertNotNull(userResponses);
        assertEquals(6, userResponses.size());

        verify(userRepository, times(6)).save(any(User.class));
    }

    @Test
    void existsByEmail_testIsEmailUnique() {
        // Arrange
        when(userRepository.existsByEmail(EMAIL)).thenReturn(false);

        // Act
        boolean result = userService.existsByEmail(EMAIL);

        // Assert
        assertFalse(result);
    }
}
