package com.dijanabojkovska.petstore.controllers;

import com.dijanabojkovska.petstore.dtos.requests.UserRequest;
import com.dijanabojkovska.petstore.dtos.responses.UserResponse;
import com.dijanabojkovska.petstore.repositories.UserRepository;
import com.dijanabojkovska.petstore.services.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.List;
import java.util.Optional;

import static com.dijanabojkovska.petstore.ObjectFactory.*;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@WebMvcTest(UserController.class)
public class UserControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private UserService userService;

    @Test
    void getUserById_ExistingUser_ReturnsUserResponse() throws Exception {
        UserResponse userResponse = createUserResponse();

        when(userService.getUserById(ID)).thenReturn(Optional.of(userResponse));

        mockMvc.perform(MockMvcRequestBuilders
                        .get("/api/users/{id}", ID)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().json(objectMapper.writeValueAsString(userResponse)));
    }

    @Test
    void getUserById_NonExistingUser_ReturnsNotFound() throws Exception {
        when(userService.getUserById(NON_EXISTING_ID)).thenReturn(Optional.empty());

        mockMvc.perform(MockMvcRequestBuilders
                        .get("/api/users/{id}", NON_EXISTING_ID)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isNotFound());
    }

    @Test
    void getAllUsers_ReturnsUserResponseList() throws Exception {
        List<UserResponse> userResponses = createUserResponseList();

        when(userService.getAllUsers()).thenReturn(userResponses);

        mockMvc.perform(MockMvcRequestBuilders
                        .get("/api/users")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().json(objectMapper.writeValueAsString(userResponses)));
    }

    @Test
    void createUser_ReturnsCreatedStatus() throws Exception {
        UserRequest userRequest = createUserRequest();
        UserResponse userResponse = createUserResponse();

        when(userService.createUser(userRequest)).thenReturn(userResponse);

        mockMvc.perform(MockMvcRequestBuilders
                        .post("/api/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(userRequest)))
                .andExpect(MockMvcResultMatchers.status().isCreated());
    }

    @Test
    void createRandomUsers_ReturnsUserResponseList() throws Exception {
        List<UserResponse> userResponses = createUserResponseList();

        when(userService.createRandomUsers()).thenReturn(userResponses);

        mockMvc.perform(MockMvcRequestBuilders
                        .get("/api/users/randomUsers")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().json(objectMapper.writeValueAsString(userResponses)));
    }
}
