package com.dijanabojkovska.petstore.controllers;

import com.dijanabojkovska.petstore.dtos.requests.PetRequest;
import com.dijanabojkovska.petstore.dtos.responses.PetResponse;
import com.dijanabojkovska.petstore.services.impl.PetServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import static com.dijanabojkovska.petstore.ObjectFactory.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(PetController.class)
public class PetControllerTest {

    @MockBean
    private PetServiceImpl petService;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void getPetById_ExistingPet_ReturnsPetResponse() throws Exception {
        PetResponse petResponse = createCatResponse();

        when(petService.getPetById(ID)).thenReturn(Optional.of(petResponse));

        mockMvc.perform(MockMvcRequestBuilders
                        .get("/api/pets/{id}", ID)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(ID));
    }

    @Test
    void getPetById_NonExistingPet_ReturnsNotFound() throws Exception {
        when(petService.getPetById(NON_EXISTING_ID)).thenReturn(Optional.empty());

        mockMvc.perform(MockMvcRequestBuilders
                        .get("/api/pets/{id}", NON_EXISTING_ID)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isNotFound());
    }

    @Test
    void getAllPets_ReturnsPetResponseList() throws Exception {
        Collection<PetResponse> petResponses = createPetResponseList();
        when(petService.getAllPets()).thenReturn(petResponses);

        mockMvc.perform(get("/api/pets")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content()
                        .json(objectMapper.writeValueAsString(petResponses)));
    }

    @Test
    void createPet_ReturnsCreatedStatus() throws Exception {
        PetRequest petRequest = createDogRequest();
        PetResponse petResponse = createDogResponse();

        when(petService.createPet(petRequest)).thenReturn(petResponse);
        mockMvc.perform(MockMvcRequestBuilders
                        .post("/api/pets")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(petRequest)))
                .andExpect(MockMvcResultMatchers.status().isCreated());
    }

    @Test
    void buyPet_ReturnsOkStatus() throws Exception {
        mockMvc.perform(get("/api/pets/purchases"))
                .andExpect(status().isOk());
    }

    @Test
    void createRandomPets_ReturnsPetResponseList() throws Exception {
        List<PetResponse> expectedResponse = createPetResponseList();

        Mockito.when(petService.createRandomPets()).thenReturn(expectedResponse);

        mockMvc.perform(MockMvcRequestBuilders
                        .get("/api/pets/randomPets")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().json(objectMapper.writeValueAsString(expectedResponse)));
    }
}

