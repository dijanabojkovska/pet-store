package com.dijanabojkovska.petstore;

import com.dijanabojkovska.petstore.dtos.requests.PetRequest;
import com.dijanabojkovska.petstore.dtos.requests.UserRequest;
import com.dijanabojkovska.petstore.dtos.responses.PetResponse;
import com.dijanabojkovska.petstore.dtos.responses.UserResponse;
import com.dijanabojkovska.petstore.models.Cat;
import com.dijanabojkovska.petstore.models.Dog;
import com.dijanabojkovska.petstore.models.Pet;
import com.dijanabojkovska.petstore.models.User;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public final class ObjectFactory {

    public static final String DATE_TIME_FORMAT = "yyyy-MM-dd";
    public static final Long ID = 1L;

    public static final String RANDOM = "random";

    public static final Long NON_EXISTING_ID = 2L;

    public static final String EMAIL = "test@example.com";

    public ObjectFactory() {

    }

    public static PetRequest createDogRequest() {
        PetRequest petRequest = new PetRequest();
        petRequest.setType("dog");
        petRequest.setName("Dog");
        petRequest.setDescription("Labrador");
        petRequest.setBirthDate("2020-10-12");
        petRequest.setRating(9);
        return petRequest;
    }

    public static PetRequest createCatRequest() {
        PetRequest petRequest = new PetRequest();
        petRequest.setType("Cat");
        petRequest.setName("Cat");
        petRequest.setDescription("Persian cat");
        petRequest.setBirthDate("2020-10-12");
        return petRequest;
    }

    public static Dog createDog() {
        Dog dog = new Dog();
        dog.setId(1L);
        dog.setName("Dog");
        dog.setDescription("Labrador");
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_TIME_FORMAT);
        LocalDate birthDate = LocalDate.parse("2020-10-12", formatter);
        dog.setBirthDate(birthDate);
        dog.setRating(9);
        dog.setPrice(BigDecimal.valueOf(12));
        return dog;
    }

    public static Cat createCat() {
        Cat cat = new Cat();
        cat.setId(1L);
        cat.setName("Cat");
        cat.setDescription("Persian");
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_TIME_FORMAT);
        LocalDate birthDate = LocalDate.parse("2020-10-12", formatter);
        cat.setBirthDate(birthDate);
        cat.setPrice(BigDecimal.valueOf(3));
        return cat;
    }

    public static PetResponse createDogResponse() {
        PetResponse dogResponse = new PetResponse();
        dogResponse.setId(1L);
        dogResponse.setName("Dog");
        dogResponse.setDescription("Labrador");
        dogResponse.setBirthDate("2020-10-12");
        dogResponse.setRating(9);
        dogResponse.setPrice(BigDecimal.valueOf(12));
        return dogResponse;
    }

    public static PetResponse createCatResponse() {
        PetResponse catResponse = new PetResponse();
        catResponse.setId(1L);
        catResponse.setName("Cat");
        catResponse.setDescription("Persian");
        catResponse.setBirthDate("2020-10-12");
        catResponse.setPrice(BigDecimal.valueOf(3));
        return catResponse;
    }

    public static List<PetResponse> createPetResponseList() {
        List<PetResponse> petResponseList = new ArrayList<>();
        petResponseList.add(createDogResponse());
        petResponseList.add(createCatResponse());
        return petResponseList;
    }

    public static List<Pet> createPetList() {
        List<Pet> petList = new ArrayList<>();
        petList.add(createDog());
        petList.add(createCat());
        return petList;
    }

    public static User createUser() {
        User user = new User();
        user.setId(1L);
        user.setFirstName("John");
        user.setLastName("Doe");
        user.setEmail("john.doe@gmail.com");
        user.setBudget(BigDecimal.valueOf(50L));
        return user;
    }

    public static UserRequest createUserRequest() {
        UserRequest userRequest = new UserRequest();
        userRequest.setFirstName("John");
        userRequest.setLastName("Doe");
        userRequest.setEmail("john.doe@gmail.com");
        userRequest.setBudget(BigDecimal.valueOf(50L));
        return userRequest;
    }

    public static UserResponse createUserResponse() {
        UserResponse userResponse = new UserResponse();
        userResponse.setId(ID);
        userResponse.setFirstName("John");
        userResponse.setLastName("Doe");
        userResponse.setEmail("john.doe@gmail.com");
        userResponse.setBudget(BigDecimal.valueOf(50L));
        return userResponse;
    }

    public static List<UserResponse> createUserResponseList() {
        List<UserResponse> userResponseList = new ArrayList<>();
        userResponseList.add(createUserResponse());
        return userResponseList;
    }

    public static List<User> createUserList() {
        List<User> userList = new ArrayList<>();
        userList.add(createUser());
        userList.add(createUser());
        return userList;
    }
}
