package com.dijanabojkovska.petstore.repositories;

import com.dijanabojkovska.petstore.models.HistoryLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface HistoryLogRepository extends JpaRepository<HistoryLog, Long> {
}
