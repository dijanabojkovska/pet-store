package com.dijanabojkovska.petstore.validators;

import com.dijanabojkovska.petstore.annotations.ValidPetType;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

public class PetTypeValidator implements ConstraintValidator<ValidPetType, String> {

    @Override
    public boolean isValid(String petType, ConstraintValidatorContext context) {
        return petType != null && (petType.equalsIgnoreCase("cat") || petType.equalsIgnoreCase("dog"));
    }
}