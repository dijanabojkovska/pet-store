package com.dijanabojkovska.petstore.mappers;

import com.dijanabojkovska.petstore.dtos.requests.PetRequest;
import com.dijanabojkovska.petstore.dtos.responses.PetResponse;
import com.dijanabojkovska.petstore.models.Cat;
import com.dijanabojkovska.petstore.models.Dog;
import com.dijanabojkovska.petstore.models.Pet;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface PetMapper {

    Dog mapPetRequestToDog(PetRequest petRequest);

    Cat mapPetRequestToCat(PetRequest petRequest);

    @Mapping(target = "price", expression = "java(cat.calculatePrice())")
    PetResponse mapCatToPetResponse(Cat cat);

    @Mapping(target = "price", expression = "java(dog.calculatePrice())")
    PetResponse mapDogToPetResponse(Dog dog);

    default PetResponse mapPetToPetResponse(Pet pet) {
        if (pet instanceof Dog) {
            return mapDogToPetResponse((Dog) pet);
        } else if (pet instanceof Cat) {
            return mapCatToPetResponse((Cat) pet);
        }
        return null;
    }

    List<PetResponse> mapCatsAndDogsListToPetResponseList(List<Pet> petList);
}
