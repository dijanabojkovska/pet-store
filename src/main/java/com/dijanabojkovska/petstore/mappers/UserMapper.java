package com.dijanabojkovska.petstore.mappers;

import com.dijanabojkovska.petstore.dtos.requests.UserRequest;
import com.dijanabojkovska.petstore.dtos.responses.UserResponse;
import com.dijanabojkovska.petstore.models.User;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface UserMapper {
    UserResponse mapUserToUserResponse(User user);

    User mapUserRequestToUser(UserRequest userRequest);

    List<UserResponse> mapUserListToUserResponseList(List<User> userList);
}
