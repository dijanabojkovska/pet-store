package com.dijanabojkovska.petstore.annotations;

import com.dijanabojkovska.petstore.validators.EmailValidator;
import jakarta.validation.Constraint;
import jakarta.validation.Payload;

import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = EmailValidator.class)
@Target({ElementType.METHOD, ElementType.FIELD, ElementType.ANNOTATION_TYPE, ElementType.CONSTRUCTOR, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
public @interface Email {

    String message() default "Invalid email address. Domain must end with '.' followed by at least one character.";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}