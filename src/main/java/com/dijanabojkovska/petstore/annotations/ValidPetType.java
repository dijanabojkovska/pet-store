package com.dijanabojkovska.petstore.annotations;

import com.dijanabojkovska.petstore.validators.PetTypeValidator;
import jakarta.validation.Constraint;
import jakarta.validation.Payload;

import java.lang.annotation.*;

@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = PetTypeValidator.class)
public @interface ValidPetType {
    String message() default "Invalid pet type. Must be 'cat' or 'dog'";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}