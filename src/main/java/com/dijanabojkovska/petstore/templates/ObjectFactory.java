package com.dijanabojkovska.petstore.templates;

public class ObjectFactory {
    public static final String PET_DOES_NOT_EXIST = "Pet with id %d does not exist";
    public static final String RATING_CANNOT_BE_NULL = "Rating can not be null, for type dog.";
    public static final String DOG = "Dog";
    public static final String CAT = "Cat";
    public static final String CAT_BOUGHT_SUCCESS_MESSAGE = "Meow, cat: %s has owner: %s %s";
    public static final String DOG_BOUGHT_SUCCESS_MESSAGE = "Woof, dog: %s has owner: %s %s";
    public static final int MIN_YEAR = 2010;
    public static final int MAX_YEAR = 2022;
    public static final int MAX_MONTH = 12;
    public static final int MAX_DAY = 28;
    public static final String USER_DOES_NOT_EXIST = "User with id %d does not exist";
}
