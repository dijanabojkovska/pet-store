package com.dijanabojkovska.petstore.dtos.responses;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PetResponse {
    private Long id;
    private UserResponse owner;
    private String name;
    private String description;
    private String birthDate;
    private BigDecimal price;
    private Integer rating;
}
