package com.dijanabojkovska.petstore.dtos.requests;

import com.dijanabojkovska.petstore.annotations.ValidPetType;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

@Data
public class PetRequest {
    @ValidPetType
    private String type;
    @NotNull(message = "The pet name cannot be null.")
    @NotEmpty(message = "The pet name cannot be empty.")
    private String name;
    private String description;
    @NotNull(message = "The birth date cannot be null.")
    @NotEmpty(message = "The birth date cannot be empty.")
    @DateTimeFormat(pattern = "yyyy-MM-dd", iso = DateTimeFormat.ISO.DATE)
    private String birthDate;
    @Min(value = 0, message = "Rating must be at least 0")
    @Max(value = 10, message = "Rating must be at most 10")
    private Integer rating;
}
