package com.dijanabojkovska.petstore.dtos.requests;

import com.dijanabojkovska.petstore.annotations.Email;
import com.dijanabojkovska.petstore.annotations.UniqueEmail;
import jakarta.validation.constraints.DecimalMin;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class UserRequest {
    @NotEmpty(message = "First name of user cannot be empty.")
    @NotNull(message = "First name of user cannot be null.")
    private String firstName;
    @NotEmpty(message = "Last name of user cannot be empty.")
    @NotNull(message = "Last name of user cannot be null.")
    private String lastName;
    @Email(message = "Invalid email address. Must contain '@' and domain must end with '.' followed by at least one character.")
    @UniqueEmail(message = "Email is already in use")
    private String email;
    @DecimalMin(value = "0.0", message = "Budget cannot be negative.")
    private BigDecimal budget;
}
