package com.dijanabojkovska.petstore.services;

import com.dijanabojkovska.petstore.dtos.requests.PetRequest;
import com.dijanabojkovska.petstore.dtos.responses.PetResponse;

import java.util.Collection;
import java.util.Optional;

public interface PetService {
    Optional<PetResponse> getPetById(Long id);

    Collection<PetResponse> getAllPets();

    PetResponse createPet(PetRequest petRequest);

    Collection<PetResponse> createRandomPets();

    void buy();
}
