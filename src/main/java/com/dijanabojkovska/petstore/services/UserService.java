package com.dijanabojkovska.petstore.services;

import com.dijanabojkovska.petstore.dtos.requests.UserRequest;
import com.dijanabojkovska.petstore.dtos.responses.UserResponse;

import java.util.Collection;
import java.util.Optional;

public interface UserService {
    Collection<UserResponse> getAllUsers();

    Optional<UserResponse> getUserById(Long id);

    UserResponse createUser(UserRequest userRequest);

    Collection<UserResponse> createRandomUsers();

    boolean existsByEmail(String email);
}
