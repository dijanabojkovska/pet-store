package com.dijanabojkovska.petstore.services.impl;

import com.dijanabojkovska.petstore.dtos.requests.UserRequest;
import com.dijanabojkovska.petstore.dtos.responses.UserResponse;
import com.dijanabojkovska.petstore.mappers.UserMapper;
import com.dijanabojkovska.petstore.models.User;
import com.dijanabojkovska.petstore.repositories.UserRepository;
import com.dijanabojkovska.petstore.services.UserService;
import com.github.javafaker.Faker;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;

import static com.dijanabojkovska.petstore.templates.ObjectFactory.USER_DOES_NOT_EXIST;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final UserMapper userMapper;
    public Faker faker;
    private final Random random = new Random();

    public UserServiceImpl(UserRepository userRepository, UserMapper userMapper) {
        this.userRepository = userRepository;
        this.userMapper = userMapper;
        this.faker = new Faker(new Locale("en-US"));
    }

    @Override
    public Optional<UserResponse> getUserById(Long id) {
        String message = String.format(USER_DOES_NOT_EXIST, id);
        User user = userRepository.findById(id).orElseThrow(() -> new NoSuchElementException(message));
        return Optional.of(userMapper.mapUserToUserResponse(user));
    }

    @Override
    public Collection<UserResponse> getAllUsers() {
        Collection<User> userList = userRepository.findAll().stream().toList();
        return userMapper.mapUserListToUserResponseList(userList.stream().toList());
    }

    @Override
    public UserResponse createUser(UserRequest userRequest) {
        User user = userRepository.save(userMapper.mapUserRequestToUser(userRequest));
        return userMapper.mapUserToUserResponse(user);
    }

    @Override
    public Collection<UserResponse> createRandomUsers() {
        List<UserResponse> userResponseList = new ArrayList<>();
        int numUsers = random.nextInt(10) + 1;
        for (int i = 1; i <= numUsers; i++) {
            User user = new User();
            user.setFirstName(faker.name().firstName());
            user.setLastName(faker.name().lastName());
            user.setEmail(faker.internet().emailAddress());
            user.setBudget(generateRandomBudget());
            userRepository.save(user);
            UserResponse userResponse = userMapper.mapUserToUserResponse(user);
            userResponseList.add(userResponse);
        }
        return userResponseList;
    }

    @Override
    public boolean existsByEmail(String email) {
        return userRepository.existsByEmail(email);
    }

    private BigDecimal generateRandomBudget() {
        Random random = new Random();
        int dollars = random.nextInt(50);
        int cents = random.nextInt(100);
        return new BigDecimal(String.format("%d.%02d", dollars, cents));
    }
}
