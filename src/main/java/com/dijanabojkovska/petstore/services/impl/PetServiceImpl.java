package com.dijanabojkovska.petstore.services.impl;

import com.dijanabojkovska.petstore.dtos.requests.PetRequest;
import com.dijanabojkovska.petstore.dtos.responses.PetResponse;
import com.dijanabojkovska.petstore.exceptions.InvalidRequestException;
import com.dijanabojkovska.petstore.mappers.PetMapper;
import com.dijanabojkovska.petstore.models.*;
import com.dijanabojkovska.petstore.repositories.HistoryLogRepository;
import com.dijanabojkovska.petstore.repositories.PetRepository;
import com.dijanabojkovska.petstore.repositories.UserRepository;
import com.dijanabojkovska.petstore.services.PetService;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;

import static com.dijanabojkovska.petstore.templates.ObjectFactory.*;

@Service
public class PetServiceImpl implements PetService {
    private final PetRepository petRepository;
    private final UserRepository userRepository;
    private final HistoryLogRepository historyLogRepository;
    private final PetMapper petMapper;
    private final Random random = new Random();

    public PetServiceImpl(PetRepository petRepository, UserRepository userRepository, HistoryLogRepository historyLogRepository, PetMapper petMapper) {
        this.petRepository = petRepository;
        this.userRepository = userRepository;
        this.historyLogRepository = historyLogRepository;
        this.petMapper = petMapper;
    }

    @Override
    public Optional<PetResponse> getPetById(Long id) {
        String message = String.format(PET_DOES_NOT_EXIST, id);
        Pet pet = petRepository.findById(id).orElseThrow(() -> new NoSuchElementException(message));
        if (pet instanceof Dog) {
            return Optional.of(petMapper.mapDogToPetResponse((Dog) pet));
        } else {
            return Optional.of(petMapper.mapCatToPetResponse((Cat) pet));
        }
    }

    @Override
    public Collection<PetResponse> getAllPets() {
        Collection<Pet> petList = petRepository.findAll().stream().toList();
        return petMapper.mapCatsAndDogsListToPetResponseList(petList.stream().toList());
    }

    @Override
    public PetResponse createPet(PetRequest petRequest) {
        return (petRequest.getType().equalsIgnoreCase(DOG))
                ? createDog(petRequest)
                : createCat(petRequest);
    }

    @Override
    public void buy() {
        List<Pet> petList = petRepository.findAll();
        List<User> userList = userRepository.findAll();
        Set<User> successfulBuyers = new HashSet<>();
        for (User user : userList) {
            for (Pet pet : petList) {
                if (petCanBePurchased(pet, user.getBudget())) {
                    completePurchase(user, pet);
                    successfulBuyers.add(user);
                }
            }
        }
        updateHistoryLog(successfulBuyers, userList);
    }

    @Override
    public List<PetResponse> createRandomPets() {
        List<PetResponse> petResponseList = new ArrayList<>();
        int numPets = random.nextInt(10) + 1;
        for (int i = 1; i <= numPets; i++) {
            Pet pet = random.nextBoolean() ? new Cat() : new Dog();
            String name = (pet instanceof Cat ? CAT : DOG) + generateRandomName();
            pet.setName(name);
            pet.setDescription(generateRandomDescription());
            pet.setBirthDate(generateRandomBirthday());
            if (pet instanceof Dog) {
                ((Dog) pet).setRating(random.nextInt(11));
            }
            BigDecimal price = pet.calculatePrice();
            pet.setPrice(price);
            petRepository.save(pet);
            PetResponse petResponse = petMapper.mapPetToPetResponse(pet);
            petResponseList.add(petResponse);
        }
        return petResponseList;
    }

    private String generateRandomName() {
        return String.format(" %d", new Random().nextInt(100));
    }

    private String generateRandomDescription() {
        return String.format("Description %d", new Random().nextInt(100));
    }

    private LocalDate generateRandomBirthday() {
        int year = MIN_YEAR + (new Random().nextInt(MAX_YEAR - MIN_YEAR + 1));
        int month = (new Random()).nextInt(MAX_MONTH) + 1;
        int day = (new Random().nextInt(MAX_DAY)) + 1;
        return LocalDate.of(year, month, day);
    }

    private PetResponse createCat(PetRequest petRequest) {
        Cat cat = petMapper.mapPetRequestToCat(petRequest);
        BigDecimal price = cat.calculatePrice();
        cat.setPrice(price);
        petRepository.save(cat);
        return petMapper.mapCatToPetResponse(cat);
    }

    private PetResponse createDog(PetRequest petRequest) {
        if (petRequest.getRating() != null) {
            Dog dog = petMapper.mapPetRequestToDog(petRequest);
            BigDecimal price = dog.calculatePrice();
            dog.setPrice(price);
            petRepository.save(dog);
            return petMapper.mapDogToPetResponse(dog);
        } else {
            throw new InvalidRequestException(RATING_CANNOT_BE_NULL);
        }
    }

    private boolean petCanBePurchased(Pet pet, BigDecimal userBudget) {
        return pet.getOwner() == null && pet.calculatePrice().compareTo(userBudget) <= 0;
    }

    private void completePurchase(User user, Pet pet) {
        BigDecimal petPrice = pet.calculatePrice();
        BigDecimal newBudget = user.getBudget().subtract(petPrice);
        pet.setOwner(user);
        user.setBudget(newBudget);
        userRepository.save(user);
        petRepository.save(pet);
        printPurchaseSuccessMessage(pet);
    }

    private void printPurchaseSuccessMessage(Pet pet) {
        String message = pet.purchaseSuccessMessage();
        System.out.println(message);
    }

    private void updateHistoryLog(Set<User> successfulBuyers, List<User> userList) {
        int countSuccessful = successfulBuyers.size();
        int countNotSuccessful = userList.size() - successfulBuyers.size();
        HistoryLog historyLog = new HistoryLog();
        historyLog.setDateExecution(LocalDateTime.now());
        historyLog.setSuccessfulBuyers(countSuccessful);
        historyLog.setUnsuccessfulBuyers(countNotSuccessful);
        historyLogRepository.save(historyLog);
    }
}
