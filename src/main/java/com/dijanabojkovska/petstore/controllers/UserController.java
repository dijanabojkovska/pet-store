package com.dijanabojkovska.petstore.controllers;

import com.dijanabojkovska.petstore.dtos.requests.UserRequest;
import com.dijanabojkovska.petstore.dtos.responses.UserResponse;
import com.dijanabojkovska.petstore.services.UserService;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Positive;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Collection;
import java.util.NoSuchElementException;

import static com.dijanabojkovska.petstore.templates.ObjectFactory.USER_DOES_NOT_EXIST;

@Validated
@RestController
@RequestMapping("/api/users")
public class UserController {
    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/{id}")
    public ResponseEntity<UserResponse> getUserById(@Positive @PathVariable Long id) {
        String message = String.format(USER_DOES_NOT_EXIST, id);
        UserResponse userResponse = userService.getUserById(id).orElseThrow(
                () -> new NoSuchElementException(message));
        return new ResponseEntity<>(userResponse, HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<Collection<UserResponse>> getAllUsers() {
        Collection<UserResponse> userResponseList =
                userService.getAllUsers().stream().toList();
        return new ResponseEntity<>(userResponseList, HttpStatus.OK);
    }

    @GetMapping("/randomUsers")
    public Collection<UserResponse> createRandomUsers() {
        return userService.createRandomUsers();
    }

    @PostMapping
    public ResponseEntity<Void> createUser(@Valid @RequestBody UserRequest userRequest) {
        UserResponse userResponse = userService.createUser(userRequest);
        return ResponseEntity.created(URI.create("/api/users/" + userResponse.getId())).build();
    }
}
