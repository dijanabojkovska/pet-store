package com.dijanabojkovska.petstore.controllers;

import com.dijanabojkovska.petstore.dtos.requests.PetRequest;
import com.dijanabojkovska.petstore.dtos.responses.PetResponse;
import com.dijanabojkovska.petstore.services.PetService;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Positive;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Collection;
import java.util.NoSuchElementException;

import static com.dijanabojkovska.petstore.templates.ObjectFactory.PET_DOES_NOT_EXIST;

@Validated
@RestController
@RequestMapping("/api/pets")
public class PetController {
    private final PetService petService;

    public PetController(PetService petService) {
        this.petService = petService;
    }

    @GetMapping("/{id}")
    public ResponseEntity<PetResponse> getPetById(@Positive @PathVariable Long id) {
        String message = String.format(PET_DOES_NOT_EXIST, id);
        PetResponse petResponse = petService.getPetById(id).orElseThrow(
                () -> new NoSuchElementException(message));
        return new ResponseEntity<>(petResponse, HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<Collection<PetResponse>> getAllPets() {
        Collection<PetResponse> petResponseList =
                petService.getAllPets().stream().toList();
        return new ResponseEntity<>(petResponseList, HttpStatus.OK);
    }

    @GetMapping("/randomPets")
    public Collection<PetResponse> createRandomPets() {
        return petService.createRandomPets();
    }

    @GetMapping("/purchases")
    public ResponseEntity<Void> buy() {
        petService.buy();
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<String> createPet(@Valid @RequestBody PetRequest petRequest) {
        PetResponse petResponse = petService.createPet(petRequest);
        return ResponseEntity.created(URI.create("/api/pets/" + petResponse.getId())).build();
    }
}
