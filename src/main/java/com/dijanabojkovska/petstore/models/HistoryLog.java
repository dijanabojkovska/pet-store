package com.dijanabojkovska.petstore.models;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
public class HistoryLog {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private LocalDateTime dateExecution;
    private Integer successfulBuyers;
    private Integer unsuccessfulBuyers;

    public HistoryLog() {
    }

    public HistoryLog(Long id, LocalDateTime dateExecution, Integer successfulBuyers, Integer unsuccessfulBuyers) {
        this.id = id;
        this.dateExecution = dateExecution;
        this.successfulBuyers = successfulBuyers;
        this.unsuccessfulBuyers = unsuccessfulBuyers;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getDateExecution() {
        return dateExecution;
    }

    public void setDateExecution(LocalDateTime dateExecution) {
        this.dateExecution = dateExecution;
    }

    public Integer getSuccessfulBuyers() {
        return successfulBuyers;
    }

    public void setSuccessfulBuyers(Integer successfulBuyers) {
        this.successfulBuyers = successfulBuyers;
    }

    public Integer getUnsuccessfulBuyers() {
        return unsuccessfulBuyers;
    }

    public void setUnsuccessfulBuyers(Integer unsuccessfulBuyers) {
        this.unsuccessfulBuyers = unsuccessfulBuyers;
    }
}
