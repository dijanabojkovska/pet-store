package com.dijanabojkovska.petstore.models;

import jakarta.persistence.*;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Period;

@Entity
@Inheritance
@DiscriminatorColumn(name = "type")
@Table(name = "pet")
public class Pet {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "owner_id")
    private User owner;

    private String name;
    private String description;
    private LocalDate birthDate;
    private BigDecimal price;

    public Pet() {
    }

    public Pet(Long id, User owner, String name, String description, LocalDate birthDate, BigDecimal price) {
        this.id = id;
        this.owner = owner;
        this.name = name;
        this.description = description;
        this.birthDate = birthDate;
        this.price = price;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public BigDecimal calculatePrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Integer getAge(LocalDate on){
        Period period = Period.between(birthDate, on);
        return period.getYears();
    }

    public String purchaseSuccessMessage(){
        return "";
    }
}
