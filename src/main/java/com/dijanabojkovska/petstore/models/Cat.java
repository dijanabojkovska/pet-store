package com.dijanabojkovska.petstore.models;

import jakarta.persistence.DiscriminatorValue;
import jakarta.persistence.Entity;

import java.math.BigDecimal;
import java.time.LocalDate;

import static com.dijanabojkovska.petstore.templates.ObjectFactory.CAT_BOUGHT_SUCCESS_MESSAGE;

@Entity
@DiscriminatorValue("cat")
public class Cat extends Pet {
    public Cat() {
    }

    @Override
    public BigDecimal calculatePrice() {
        Integer age = getAge(LocalDate.now());
        return BigDecimal.valueOf(age);
    }

    @Override
    public String purchaseSuccessMessage() {
        return String.format(CAT_BOUGHT_SUCCESS_MESSAGE, getName(), getOwner().getFirstName(), getOwner().getLastName());
    }
}
