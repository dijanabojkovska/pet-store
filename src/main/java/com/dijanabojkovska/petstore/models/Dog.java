package com.dijanabojkovska.petstore.models;

import jakarta.persistence.DiscriminatorValue;
import jakarta.persistence.Entity;

import java.math.BigDecimal;
import java.time.LocalDate;

import static com.dijanabojkovska.petstore.templates.ObjectFactory.DOG_BOUGHT_SUCCESS_MESSAGE;

@Entity
@DiscriminatorValue("dog")
public class Dog extends Pet {

    private Integer rating;

    public Dog() {
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    @Override
    public BigDecimal calculatePrice() {
        Integer age = getAge(LocalDate.now());
        return BigDecimal.valueOf(age + getRating());
    }

    @Override
    public String purchaseSuccessMessage() {
        return String.format(DOG_BOUGHT_SUCCESS_MESSAGE, getName(), getOwner().getFirstName(), getOwner().getLastName());
    }
}
